/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW09_V3.sas*/
/* Date Created: 10/11/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW09 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 4 - Libname Statements 
----------------------------*/

LIBNAME orion '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW09/';

/*--------------------------
 Step 4 - Assign Orion 
----------------------------*/

/* data work.donations; */
/*    set orion.Employee_donations; */
/*    keep Employee_ID Qtr1 Qtr2 Qtr3 Qtr4; */
/*    Total=sum(Qtr1,Qtr2,Qtr3,Qtr4); */
/* run; */

/*--------------------------------------
 Step 6 - Comment with single character 
--------------------------------------*/

*proc print data=work.donations;

/*--------------------------------------
 Step 7 - Donations data -> new library 
--------------------------------------*/

/* data rkRef.donations; */
/*    set orion.Employee_donations; */
/*    keep Employee_ID Qtr1 Qtr2 Qtr3 Qtr4; */
/*    Total=sum(Qtr1,Qtr2,Qtr3,Qtr4); */
/* run; */

/*--------------------------------------
 Step 8 - Open pdf destination 
--------------------------------------*/

ods pdf file ="/folders/myfolders/HW09/rohithkarthikeyan_HW09_outputB.pdf"
bookmarkgen=no style = HTMLBlue;



/*--------------------------------------
 Step 9 - rkRef w/descriptors  
--------------------------------------*/

proc contents data=rkRef.donations; 
title 'Descriptor Portion of Donations Permanent Data Set';
run;

/*--------------------------------------
 Step 10 - work w/descriptors
--------------------------------------*/

proc contents data=work._ALL_; 
title 'Descriptor Portion of All Data Sets in Work Library';
run;

/*--------------------------------------
 Step 11 - Orion !(w/descriptors)
--------------------------------------*/

proc contents data=orion._ALL_ nods; 
title 'List of Data Sets in Orion Library';
run;

/*--------------------------------------
 Step 12 - Close pdf destination
--------------------------------------*/

ods pdf close;

/*--------------------------------------
 Step 13 - Close pdf destination
--------------------------------------*/