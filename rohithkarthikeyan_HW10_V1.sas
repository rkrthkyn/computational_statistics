/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW10_V1.sas*/
/* Date Created: 10/18/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW010 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 2 - Libname Statements 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW10/';

/*--------------------------
 Step 3 - Libname Statements 
----------------------------*/

filename outFile '/folders/myfolders/HW10/outFileRK.pdf';

/*-----------------------------------
 Step 4 - NewDataSet with formatting 
-----------------------------------*/

data rkRef.jobData;
	set srcData.jobs2017;
	if state ~= '';
	label 	Aug__2016 = 'August 2016'
			Sept__2016 = 'September 2016'
			Oct__2016 = 'October 2016'
			Nov__2016 = 'November 2016'
			Dec__2016 = 'December 2016'
			Jan__2017 = 'January 2017'
			Feb__2017 = 'February 2017'
			Mar__2017 = 'March 2017'
			Apr__2017 = 'April 2017'
			May_2017 = 'May 2017'
			June_2017 = 'June 2017'
			July_2017 = 'July 2017'
			Aug__2017 = 'August 2017';
			
	Report_Date  = '12oct2017'd;
	label Report_Date = 'Report Date';
	format Report_Date mmddyy10.;	
	Annual_Change = ((Aug__2017-Aug__2016)/Aug__2016);
	format Annual_Change percent8.1;
	label Annual_Change = 'Annual Change';
	
run;

/*-----------------------------------
 Step 5 - abs(AnnualChange) > 5%
-----------------------------------*/

data Change5Percent;
set rkRef.jobData;
if abs(Annual_Change) > 5/100;
keep  Sector State Aug__2016
		Aug__2017 Report_Date Annual_Change; 
run;

/*-----------------------------------
 Step 6 - Difference in job #
-----------------------------------*/

data JobCountPlus1;
set rkRef.jobData;
if (Aug__2017-Aug__2016) >= 1;
drop Aug__2016 Sept__2016 Oct__2016
	Nov__2016 Dec__2016 Report_Date Annual_Change;
run;

/*-----------------------------------
 Step 7 - Service Industries
-----------------------------------*/

data serviceIndustries;
set rkRef.jobData;
where Sector contains 'SERVICES';
if Annual_Change ~= '';
keep Sector State Aug__2016 Aug__2017 
	 Report_Date Annual_Change;
	 format Aug__2016 Aug__2017 COMMA.0;
run;
	
/*-----------------------------------
 Step 8 - Southern States
-----------------------------------*/

data southernStates;
set rkRef.jobData;
if Sector ~= 'Government';
where (State in ('Texas','Oklahoma','Arkansas','Louisiana',
'Mississippi','Kentucky','Alabama','Florida','Georgia',
'South Carolina','North Carolina','Virginia')) OR (State like 'District of Columbia%') 
OR (State like 'Tennessee%') ;
drop Aug__2016 Sept__2016 Oct__2016
	Nov__2016 Dec__2016 Report_Date;
run;

/*-----------------------------------
 Step 9 - Open pdf file
-----------------------------------*/

ods pdf file = outFile
 bookmarkgen=no;

/*-----------------------------------
 Step 10 - Descritor data from step 4
-----------------------------------*/

proc contents data=rkRef.jobData; 
title '#10 Descriptor Portion of Cleaned Jobs Data Set';
run;

/*-----------------------------------
 Step 11 - List of temp data
-----------------------------------*/

proc contents data=work._ALL_ nods; 
title '#11 List of Temporary Data Sets';
run;

/*-----------------------------------
 Step 12 - Step 5 Data
-----------------------------------*/

proc print data=work.Change5Percent NOOBS LABEL;
title '#12 Records with over 5% Annual Change';
*var Report_Date; 
var Annual_Change State Sector Aug__2017 Aug__2016;
run;

/*-----------------------------------
 Step 13 - Datasets from 7-8
-----------------------------------*/

proc print data=work.JobCountPlus1 NOOBS LABEL;
title '#13 Records with Recent Monthly Increase';
run;

proc print data=work.serviceIndustries LABEL;
title '#13 Services';
var State Aug__2016 Aug__2017 Annual_Change Sector Report_Date;
run;

proc print data=work.southernStates NOOBS LABEL;
title '#13 Southern States';
run;

ods pdf close;