#rohithkarthikeyan_HW07_Script
#File Directory: "C:/Users/Rohith/Syncplicity Folders/Course-work/STAT 604/HW07"
#Created by Rohith Karthikeyan
#Date: 10/02/2017
#Tasks for HW07-STAT 604
#Last Executed: 10/02/2017

Sys.time()

# =====================
# Step 1 - Housekeeping
# =====================

ls()
rm(list = ls())

# =====================
# Step 2 - LoadHW04Data
# =====================

load("C:/Users/Rohith/Syncplicity Folders/Course-work/STAT 604/HW07/HW04.RData")
ls()

# =====================
# Step 3 - GraphicsO/P
# =====================

#do this in the end

# =====================
# Step 4 - Histograms
# =====================

# (a) With default breaks

{hist(Oklahoma$PTRatio,freq =FALSE,
main ="Pupil/Teacher Ratios in Oklahoma Schools",
xlab ="Pupils/Teacher",ylab ="Density");}

# (b) Forced breaks

BrkVec<-seq(0,length(Oklahoma$PTRatio),5)
hist(Oklahoma$PTRatio,breaks = BrkVec,freq =FALSE,
main ="Pupil/Teacher Ratios in Oklahoma Schools",
xlab ="Pupils/Teacher",ylab ="Density",xlim =c(0,150));

# =====================
# Step 5-Maroon NDist
# =====================

PTx <- seq(0,150,1;
meanPT <- mean(Oklahoma$PTRatio,na.rm=TRUE);
sdPT <- sd(Oklahoma$PTRatio,na.rm= TRUE);
PTRatioNorm<-dnorm(PTx,meanPT,sdPT);
lines(PTx,PTRatioNorm, col="maroon");

# =====================
# Step 6 - VerticalLine
# =====================

abline(v=mean(Oklahoma$PTRatio,na.rm=TRUE),col= palette()[5]);

# ===========================
# Step 7 - TeachersVsPTRatio
# ===========================


{plot(Oklahoma$Teachers, Oklahoma$PTRatio, pch=3,
 col ="#FF9900", xlim =c(0,140),xlab = "Teachers", 
ylab ="Pupil/Teacher Ratio");}

# ===========================
# Step 8 - Linear Model
# ===========================

PTR <- Oklahoma$PTRatio[na.rm=TRUE];
LinModel <-lm(PTR~Teachers,Oklahoma);
abline(LinModel, col="purple");
summary(LinModel);


# ===========================
# Step 9 - Imbed Date/Time
# ===========================

text(100,145,bquote(.(Sys.time())));

# ===========================
# Step 10 - BoxPlot
# ===========================
PlotNames <-seq(7,12,1);
{boxplot(Oklahoma$Grade7,Oklahoma$Grade8,Oklahoma$Grade9,
Oklahoma$Grade10,Oklahoma$Grade11,Oklahoma$Grade12,
range=0, names =PlotNames,ylim=c(0,1200),
col="light green",xlab ="Grades",ylab ="Students",
main ="Tulsa County vs. State")}

# ===========================
# Step 11 - Add diamonds
# ===========================

{TulsaMeans <-colMeans(OKGrades[Oklahoma$County == "TULSA COUNTY",],
na.rm=TRUE);}
points(TulsaMeans,pch=23,cex = 2,col="red",bg ="dark green")



