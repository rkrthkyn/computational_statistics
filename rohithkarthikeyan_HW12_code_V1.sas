/***********************************************************************************/
/* Program Name: rohithkarthikeyan_HW13_code_V1.sas*/
/* Date Created: 11/13/2017 */
/* Author: Rohith Karthikeyan*/
/* Purpose: HW13 - STAT 604 */
/***********************************************************************************/

/*--------------------------
 Step 1 - Libname Statement 
----------------------------*/

LIBNAME srcData '/folders/myfolders/PRG1_Data_and_Programs/PRG1 Data and Programs (New)/' ACCESS = READONLY;
LIBNAME rkRef '/folders/myfolders/HW12/';

/*--------------------------
 Step 2 - Clean DataSet 
----------------------------*/

data rkRef.cleanData (keep = zip primary_city State timezone county estimated_population);
length county $31;
set srcData.zip_codes;
*limit county length;
*remove decommissioned zipcodes;
if decommissioned ~= '1';
*remove states that match criteria;
if state ~= 'AA';
if state ~= 'AE';
if state ~= 'AP';
* (2.a): remove suffixes from county data;
*county;
if scan(county, -1) = 'County' then do;
call scan(county, -1, pos, length);
county = substr(county,1,pos-2);
end;
*parish;
if scan(county, -1) = 'Parish' then do;
call scan(county, -1, pos, length);
county = substr(county,1,pos-2);
end;
*borough;
if scan(county, -1) = 'Borough' then do;
call scan(county, -1, pos, length);
county = substr(county,1,pos-2);
end;
*(2.b) Convert to numeric value;
estimated_popultion = input(estimated_population,20.2);
*(2.c)Replace Underscores;
timezone = translate(timezone,' ','_');
label primary_city = 'City';
label state = 'State';
label timezone = 'Time Zone';
label county = 'County';
label estimated_population = 'Est. Population';
label zip ='Zip Code';
run;

/*--------------------------
 Step 3 - Summarizing Data
----------------------------*/

*(3.a)sort cleanData by primary_city;
proc sort data = rkRef.cleanData
		  out  = cleanSorted;
by primary_city state ;
run;

*(3.b);
data tempSummary(drop = zip estimated_population);
length ZipCode $ 1700; *Character length for ZipCode data;
set cleanSorted;
by primary_city;


*(3.c)/(3.d);
if estimated_population ~= '';
if First.primary_city then do;
estCityPop =0;
ZipCode ='';
retain ZipCode;
end;
estCityPop+estimated_population;
ZipCode = catX(',',ZipCode,zip);
if Last.primary_city;
label ZipCode = 'Zip Code';
label estCityPop = 'Est. City Population';
format estCityPop comma18.0;

*(3.e);
if estCityPop >0;
output;
run;

/*-------------------------------
 Step 4 - pdf destination/ Print
--------------------------------*/

filename outFile '/folders/myfolders/HW12/outFileRK.pdf';
ods pdf file = outFile bookmarkgen=no;

proc contents data = work.cleanSorted;
title '4.1 Descriptor Portion of Cleaned Zip Code Data Set';
run; 

*subset outputs;
proc print data = work.cleanSorted LABEL;
title '4.2 Cleaned Zip Codes from Selected Cities';
where (primary_city = 'Buffalo' OR primary_city = 'Center' OR primary_city ='Las
Vegas' OR primary_city = 'Bristow' OR primary_city = 'Athens' OR primary_city = 'Carolina' OR primary_city = 'Auke Bay' OR primary_city = 'Muleshoe' OR primary_city = 'Washington');
var zip primary_city state timezone county estimated_population;
run;

proc contents data = work.tempSummary;
title '4.3 Descriptor Portion of Summarized Zip Codes Data Set';
run;

proc print data = work.tempSummary LABEL;
title '4.4 Summarized Zip Codes from Selected Cities';
where (primary_city = 'Buffalo' OR primary_city = 'Center' OR primary_city ='Las
Vegas' OR primary_city = 'Bristow' OR primary_city = 'Athens' OR primary_city = 'Carolina' OR primary_city = 'Auke Bay' OR primary_city = 'Muleshoe' OR primary_city = 'Washington');
var ZipCode primary_city state timezone county estCityPop;
run;

ods pdf close;

/* Phew. Kinda messed up this time */